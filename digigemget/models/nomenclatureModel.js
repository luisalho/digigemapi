/**
 * Model: Nomenclature
 */
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

// Nomenclature Schema.
const nomenclatureSchema = new mongoose.Schema({
    cod_nmu: Number,
    desc_nmu: String,
    cod_gruppo_tec: Number,
    tipo_propr_materiale: String    
});

// Export the model.
module.exports = mongoose.model('Nomenclature', nomenclatureSchema);