module.exports = function (context, req) {
    // Database interaction.
    const mongoose = require('mongoose');
    const DATABASE = process.env.MongodbAtlas;

    // Connect to our Database and handle any bad connections
    mongoose.connect(DATABASE);
    mongoose.Promise = global.Promise; // Tell Mongoose to use ES6 promises
    mongoose.connection.on('error', (err) => {
        context.log(`ERROR→ ${err.message}`);
    });

    // Nomenclature Schema.
    require('./models/nomenclatureModel');
    const Nomenclature = mongoose.model('Nomenclature');

    Nomenclature
        .find({
            cod_nmu: 644880 // search query
        })
        .then(doc => {
            context.res = {
                body: doc
            }
        })
        .catch(err => {
            context.res = {
                status: 404,
                body: "No documents found"
            }
        })    

    context.done();
};